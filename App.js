import {SafeAreaView} from 'react-native-safe-area-context';
import React from 'react';
import {StatusBar} from 'react-native';
import Routing from './src/simpleNetworking/Routing';

const App = () => {
  return (
    <SafeAreaView style={{flex: 1}}>
      <StatusBar barStyle={'dark-content'} />
      <Routing />
    </SafeAreaView>
  );
};

export default App;
